<?php

header('Content-Type: application/json');

try{

    $logger->info("accProvincesListAll init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);
    
    $p_id = (empty($post->id)) ? "" : (int) $post->id;
    $p_supplier = (bool) $post->proveedor;
    $p_customer = (bool) $post->cliente;
    $supplier = array();
    $customer = array(); 

    //valido si existe proveedor
    $valsupplier = $contabilidad->accValidateSupplier($p_id);
    
    if($valsupplier["data"][0]["supplier"]>0){
        $supplier = $contabilidad->accDeleteSupplier($p_id);
    }

    //valido si existe cliente
    $valcustomer = $contabilidad->accValidateCustomer($p_id);
    
    if($valcustomer["data"][0]["customer"]>0){
        $customer = $contabilidad->accDeleteCustomer($p_id);
    }

    if($supplier["data"]["resp"] || $customer["data"]["resp"]){

        $data["state"]   = true;
        $data["message"] = "El registro se eliminó con éxito";

    }else{

        $data["state"]   = false;
        $data["message"] = "Error de Sistema";

    }    
   
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accProvincesListAll: ".$data);
}

echo json_encode($data);

?>