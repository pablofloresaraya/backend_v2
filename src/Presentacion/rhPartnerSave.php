<?php

header('Content-Type: application/json');

try{
    $logger->info("accAccountingSaveForm init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);

    $p_id        = (empty($post->id)) ? "" : (int) $post->id;
    $p_rut       = (empty($post->rut)) ? "" : (string) $post->rut;
    $p_dig       = (empty($post->dig)) ? "" : (string) $post->dig;
    $p_nombre    = (empty($post->nombre)) ? "" : (string) $post->nombre;
    $p_giro      = (empty($post->giro)) ? "" : (string)  $post->giro;
    $p_direccion = (empty($post->direccion)) ? "" : (string)  $post->direccion;
    $p_region    = (empty($post->region->id)) ? "" : (int) $post->region->id;
    $p_provincia = (empty($post->provincia->id)) ? "" : (int) $post->provincia->id;
    $p_comuna    = (empty($post->comuna->id)) ? "" : (int) $post->comuna->id;   
    $p_telefono  = (empty($post->telefono)) ? "" : (string) $post->telefono;
    $p_celular   = (empty($post->celular)) ? "" : (string)  $post->celular;
    $p_email     = (empty($post->email)) ? "" : (string) $post->email;
    $p_sucursal  = (empty($post->sucursal)) ? "" : (string) $post->sucursal;
    $p_sucursal_email = (empty($post->comuna)) ? "" : (string) $post->email_suc;    
    $p_proveedor = (empty($post->proveedor)) ? false : (bool) $post->proveedor;
    $p_cliente   = (empty($post->cliente)) ? false : (bool) $post->cliente;
    $p_cuenta    = (empty($post->cuenta)) ? false : (int) $post->cuenta;
    $p_usuario   = (empty($post->usuario)) ? false : (int) $post->usuario;     

    function is_valid_email($email){
        $matches = null;
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,3})$/', $email, $matches));
    }
    
    if(!empty($p_proveedor) || !empty($p_cliente)){

        $conta = 0;
        $message = "";

        $obligatorios = ['rut','nombre','direccion','region','provincia','comuna'];

        foreach($post as $key => $value){
            for($i=0;$i<count($obligatorios);$i++){
                if($key===$obligatorios[$i]){
                    if(is_object($value)){
                        if(empty($value->id)){
                            $message = $obligatorios[$i];
                            $conta++;
                            break 2;
                        }
                    }else{
                        if(empty($value)){
                            $message = $obligatorios[$i];
                            $conta++;
                            break 2;
                        }
                    }
                }
            }
        }

        if($conta===0){

            if(empty($p_id)){ //insert

                $resp = $contabilidad->accSaveProv(
                                                $p_rut,
                                                $p_dig,
                                                $p_nombre,
                                                $p_usuario,
                                                $p_comuna,
                                                $p_direccion,
                                                $p_telefono,
                                                $p_celular,
                                                $p_email,
                                                $p_giro,
                                                $p_sucursal,
                                                $p_sucursal_email
                                            );
                
                if($resp["data"]["resp"]){                            

                    //rescato id-partner                        
                    $partner=$contabilidad->accGetIdPartnerMax();                        

                    if($p_proveedor){
                        $contabilidad->accSaveSupplier($partner["data"][0]["sec"]);
                    }
                    
                    if($p_cliente){
                        $contabilidad->accSaveCustomer($partner["data"][0]["sec"]);        
                    }

                }
                
                $data["status"]  = $resp["data"]["resp"];
                $data["message"] = "La información se ingresó con éxito";
    
            }else{ //update

                $resp = $contabilidad->accUpdateProv(
                                                    $p_rut,
                                                    $p_dig,
                                                    $p_nombre,
                                                    $p_usuario,
                                                    $p_comuna,
                                                    $p_direccion,
                                                    $p_telefono,
                                                    $p_celular,
                                                    $p_email,
                                                    $p_giro,
                                                    $p_sucursal,
                                                    $p_sucursal_email,
                                                    $p_id
                                                );
                                                
                if($resp["data"]["resp"]){                                
                
                    $p_clit = ($p_cliente) ? "T" : "F";
                    $contabilidad->accUpdateCustomer($p_id,$p_clit);
                    
                    $p_prov = ($p_proveedor) ? "T" : "F";
                    $contabilidad->accUpdateSupplier($p_id,$p_prov);
                
                }

                $data["status"]  = $resp["data"]["resp"];
                $data["message"] = "La información se actualizó con éxito";                        

            }

        }else{ //campos vacios

            $data["status"]  = false;
            $data["message"] = "Ingrese ".$message;
            $data["clase"]   = "modal-header-danger";

        }
    
    }else{ //checkbox

        $data["status"]  = false;
        $data["message"] = "Debe seleccionar Cliente ó Proveedor";
        $data["clase"]   = "modal-header-danger";

    }
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engGetModuleAll: ".$data);
}

echo json_encode($data);
