<?php
try{

    $logger->info("accRegionsListAll init");    
    $regions    = $contabilidad->accRegionsListAll();
    $data = $regions;

}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accRegionsListAll: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);