<?php

header('Content-Type: application/json');

try{

    $logger->info("accReportBookBig init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);
    
    $p_opcion    = (empty($post->opcion))    ? "" : (string) $post->opcion;
    $p_cuenta    = (empty($post->cuenta))    ? "" : (int) $post->cuenta;
    $p_fechaini  = (empty($post->fechaini))  ? "" : (string) date("d-m-Y", strtotime($post->fechaini));
    $p_fechafin  = (empty($post->fechafin))  ? "" : (string) date("d-m-Y", strtotime($post->fechafin));
    $p_periodo   = (empty($post->periodo))   ? "" : (string) date("m-Y", strtotime($post->periodo));
    $p_cuentagen = (empty($post->cuentagen)) ? "" : (int) $post->cuentagen;

    $path  = false;
    $conta = 0;

    $file = ($p_opcion==='P') ? '/accReportPdfBookBig.php' : '/accReportExcelBookBig.php';

    if(!empty($file) && !empty($p_cuenta)){       

        if(empty($p_periodo)){ //reporte x fechas
            
            if(!empty($p_fechaini) && !empty($p_fechafin)){
                
                $account_cabecera = $contabilidad->accValDateReportBookBig($p_fechaini, $p_fechafin);

                if(!empty($account_cabecera['data'])){ //existe periodo

                    for($i=0; $i<count($account_cabecera['data']);$i++){

                        //valido cuenta x periodo
                        $account_detalles = $contabilidad->accValAccountReportBookBigDet($account_cabecera['data'][$i]['id'], $p_cuenta);

                        for($n=0; $n<count($account_detalles['data']);$n++){

                            if(!empty($account_detalles['data'][$n]['account_id'])){
                                $conta++;
                            }

                        }

                    }

                }    

                if($conta>0){
                    $path = $file."?t=".$param_t."&c=".$_GET["c"]."&i=".base64_encode($p_fechaini)."&f=".base64_encode($p_fechafin)."&p=".base64_encode($p_periodo)."&a=".base64_encode($p_cuenta)."&g=".base64_encode($p_cuentagen);
                }

            }

        }else{ //reporte x periodo

            if(!empty($p_periodo)){

                $account_cabecera = $contabilidad->accValPeriodReportBookBig($p_periodo);
                
                if(!empty($account_cabecera['data'])){ //existe periodo

                    for($i=0; $i<count($account_cabecera['data']);$i++){

                        //valido periodo x cuenta
                        $account_detalles = $contabilidad->accValAccountReportBookBigDet($account_cabecera['data'][$i]['id'], $p_cuenta);
                        
                        for($n=0; $n<count($account_detalles['data']);$n++){
                            if(!empty($account_detalles['data'][$n]['account_id'])){
                                $conta++;
                            }    
                        }

                    }

                    if($conta>0){                    
                        $path = $file."?t=".$param_t."&c=".$param_c."&i=".base64_encode($p_fechaini)."&f=".base64_encode($p_fechafin)."&p=".base64_encode($p_periodo)."&a=".base64_encode($p_cuenta)."&g=".base64_encode($p_cuentagen);
                    }    
                
                }

            }

        }        

    }
    
    $data = $path;

}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accReportBookBig: ".$data);
}

echo json_encode($data);
