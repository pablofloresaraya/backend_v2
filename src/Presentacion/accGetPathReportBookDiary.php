<?php

header('Content-Type: application/json');

try{

    $logger->info("accReportBookDiary init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);
    
    $p_opcion   = (empty($post->opcion))   ? "" : (string) $post->opcion;
    $p_fechaini = (empty($post->fechaini)) ? "" : (string) date("d-m-Y", strtotime($post->fechaini));
    $p_fechafin = (empty($post->fechafin)) ? "" : (string) date("d-m-Y", strtotime($post->fechafin));
    $p_periodo  = (empty($post->periodo))  ? "" : (string) date("m-Y", strtotime($post->periodo));
    $p_cuenta   = (empty($post->cuenta))   ? "" : (int) $post->cuenta;

    $path=false;

    $file = ($p_opcion==='P') ? '/accReportPdfBookDiary.php' : '/accReportExcelBookDiary.php';

    if(!empty($file)){

        if(empty($p_periodo)){ //reporte x fechas

            if(!empty($p_fechaini) && !empty($p_fechafin)){

                $valida=$contabilidad->accValReportDiaryBookDate($p_fechaini, $p_fechafin);

                if($valida['data'][0]['reports']>0){
                    $path = $file."?t=".$param_t."&c=".$_GET["c"]."&i=".base64_encode($p_fechaini)."&f=".base64_encode($p_fechafin)."&p=".base64_encode($p_periodo)."&g=".base64_encode($p_cuenta);
                }

            }

        }else{ //reporte x periodo

            if(!empty($p_periodo)){

                $valida=$contabilidad->accValReportDiaryBookPeriod($p_periodo);
                
                if($valida['data'][0]['reports']>0){
                    $path = $file."?t=".$param_t."&c=".$param_c."&i=".base64_encode($p_fechaini)."&f=".base64_encode($p_fechafin)."&p=".base64_encode($p_periodo)."&g=".base64_encode($p_cuenta);
                }

            }

        }

    }
    
    $data = $path;

}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accReportBookDiary: ".$data);
}

echo json_encode($data);









