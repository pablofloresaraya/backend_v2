<?php

try{
    $logger->info("accRutTypeListAll init");    
    $ruts = $contabilidad->accRutTypeListAll();
    $data = $ruts;
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accRutTypeListAll: ".$data);
}

header('Content-Type: application/json');
echo json_encode($data);
