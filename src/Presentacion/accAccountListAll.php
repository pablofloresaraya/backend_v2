<?php
try{
    $logger->info("accAccountListAll init");
    $p_nombre = (empty($_POST["var1"]))? "":(string) $_POST["var1"];
    $data = $contabilidad->accAccountListAll($p_nombre);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engGetModuleAll: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>