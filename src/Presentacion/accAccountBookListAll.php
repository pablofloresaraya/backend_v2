<?php

try{
    $logger->info("accGetAccounts init");    
    $accounts = $contabilidad->accGetAccounts();
    $data = $accounts;
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accGetAccounts: ".$data);
}

header('Content-Type: application/json');
echo json_encode($data);