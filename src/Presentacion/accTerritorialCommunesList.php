<?php

header('Content-Type: application/json');

try{

    $logger->info("accCommunesListAll init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);

    $p_provincia = (empty($post->provincia)) ? "" : (int) $post->provincia;

    $communes = $contabilidad->accCommunesListAll($p_provincia);
    $data = $communes;

}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accCommunesListAll: ".$data);
}

echo json_encode($data);
?>