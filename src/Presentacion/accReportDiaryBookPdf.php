<?php

header('Content-Type: application/json');

try{

    $logger->info("accReporDiaryBookPdf init"); 
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);
    
    $p_fechaini = (empty($post->fechaini)) ? "" : (string) $post->fechaini;
    $p_fechafin = (empty($post->fechafin)) ? "" : (string) $post->fechafin;
    $p_periodo  = (empty($post->periodo))  ? "" : (string) $post->periodo;

    $cabecera = array();
    $detalles = array();

    if(empty($p_periodo)){ //reporte por fechas

        $cabecera = $contabilidad->accReportDiaryBookDate($p_fechaini,$p_fechafin);

    }else{ //reporte por periodo

        $cabecera = $contabilidad->accReportDiaryBookPeriod($p_periodo);

    }

    $data["response"] = $cabecera;
   
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accProvincesListAll: ".$data);
}

echo json_encode($data);











