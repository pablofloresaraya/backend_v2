<?php
declare(strict_types=1);

namespace App\Datos\Contabilidad;

use Psr\Log\LoggerInterface;
use App\Datos\Connection\DataBaseKlinik;

class ContabilidadSql{

    //private $dataBase;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        //$this->dataBase = new DataBaseKlinik();
        $this->logger = $logger;
    }

    //Acc_VoucherType
    public function accVoucherTypeListAll(int $p_company_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select id, company_id, voucher_type_code, name, description, class_type, create_uid, create_date
                from acc_voucher_type as a
                where company_id=:p_company_id
                order by id;";
                
        $param = array(':p_company_id' => $p_company_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accVoucherTypeView(int $p_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "SELECT * FROM acc_voucher_type WHERE id=:p_id";

        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function accVoucherTypeUpdate(int $p_id, int $p_company_id, string $p_voucher_type_code, string $p_name, string $p_description, string $p_class_type): bool{        
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "UPDATE acc_voucher_type 
                SET company_id=:p_company_id, voucher_type_code=:p_voucher_type_code, name=:p_name, description=:p_description, class_type=:p_class_type
                WHERE id=:p_id";
        
        $param = array(':p_id' => $p_id,':p_company_id' => $p_company_id,':p_voucher_type_code' => $p_voucher_type_code,':p_name' => $p_name,':p_description' => $p_description,':p_class_type' => $p_class_type);
        $result = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $result;
    }

    public function accVoucherTypeDelete(int $p_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "DELETE
                FROM acc_voucher_type 
                WHERE id=:p_id";
        
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("DELETE",$sql,$param);
        return $data;
    }

    //Acc_Account
    public function accAccountListAll(int $p_company_id,string $p_nombre): array{        
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select  regexp_matches(upper(a.name),upper(:p_nombre))
                    ,a.*,b.name type_name
                from acc_account as a
                left outer join acc_account_type as b on b.id=a.type_id
                where 1=1
                and a.company_id=:p_company_id
                order by a.account_code;";
                
        $param = array(':p_nombre' => $p_nombre, ':p_company_id' => $p_company_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    //add 19-07-21

    public function accRutTypeListAll(): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.id, a.identifier as run, a.identifier_dv, (a.identifier||'-'||a.identifier_dv) as rut, a.name as nombre from rh_partner a
                    where ( 0<(select count(*) from arc_customer where partner_id=a.id and activo=TRUE)
                            or 0<(select count(*) from aps_supplier where partner_id=a.id and activo=TRUE) )";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accRegionsListAll(): array{     //regiones 
        $dataBase = new DataBaseKlinik($this->logger);  
        
        $sql = "select id, country_id, name, code from base_territorial where parent_id IS NULL order by id ASC";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accProvincesListAll(int $p_region): array{  
        $dataBase = new DataBaseKlinik($this->logger);      
        
        $sql = "select id, country_id, name, parent_id, code from base_territorial where char_length(code)=3 and parent_id = :p_region";
                   
        $param = array(':p_region' => $p_region);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accCommunesListAll(int $p_provincia): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select id, country_id, name, parent_id, code from base_territorial where char_length(code)=5 and parent_id = :p_provincia";
                   
        $param = array(':p_provincia' => $p_provincia);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accGetProv(string $p_rut): array{  //proveedor-clientes 
        $dataBase = new DataBaseKlinik($this->logger);     
        
        $sql = "select a.*
                ,(select name from base_territorial where id = a.address_city_id and char_length(code)=5) as name_comuna
		        ,(select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) as id_provincia
		        ,(select name from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) and char_length(code)=3) as name_provincia		
		        ,(select parent_id from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) and char_length(code)=3) as id_region
		        ,(select name from base_territorial where id = (select parent_id from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) and char_length(code)=3) and char_length(code)=2) as name_region	
                ,(select count(*) from arc_customer where partner_id=a.id and activo=TRUE) as customer
                ,(select count(*) from aps_supplier where partner_id=a.id and activo=TRUE) as supplier
        from rh_partner a 
            where a.identifier=:p_rut";
                   
        $param = array(':p_rut' => $p_rut);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accValidaProv(string $p_rut): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select count(*) as valida from rh_partner where identifier=:p_rut";
                   
        $param = array(':p_rut' => $p_rut);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accSaveProv(
                                string $p_identifier,
                                string $p_dv,
                                string $p_name,
                                int $p_usuario,                                
                                int    $p_city,                               
                                string $p_direccion,
                                string $p_phone,
                                string $p_mobile,
                                string $p_email,
                                string $p_giro,
                                string $p_sucursal,
                                string $p_sucursal_email                                  
                            ): array{
        $dataBase = new DataBaseKlinik($this->logger);                                
        
        $sql = "insert into rh_partner
                (   identifier, 
                    identifier_dv, 
                    name,
                    user_id, 
                    address_country_id, 
                    address_city_id,
                    address,
                    phone,
                    mobile_phone,
                    email,
                    company_activity,
                    sucursal,
                    sucursal_email
                )
                values (
                    :p_identifier,
                    :p_dv,
                    :p_name,
                    :p_usuario,
                    (select country_id from base_territorial where id = :p_city),
                    :p_city,
                    :p_direccion,
                    :p_phone,
                    :p_mobile,
                    :p_email,
                    :p_giro,
                    :p_sucursal,
                    :p_sucursal_email                   
                )";
                   
        $param = array(
                        ':p_identifier' => $p_identifier,
                        ':p_dv' => $p_dv,
                        ':p_name' => $p_name,
                        ':p_usuario' => $p_usuario,                        
                        ':p_city' => $p_city,
                        ':p_direccion' => $p_direccion,
                        ':p_phone' => $p_phone,
                        ':p_mobile' => $p_mobile,
                        ':p_email' => $p_email,
                        ':p_giro' => $p_giro,
                        ':p_sucursal' => $p_sucursal,
                        ':p_sucursal_email' => $p_sucursal_email                         
                    );
        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;      
    }

    public function accGetIdPartnerMax(): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "select max(id) as sec from rh_partner";
        $data = $dataBase->execQueryParam("SELECT",$sql,"");
        return $data;

    }

    public function accGetIdPartner(string $p_identifier): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "select id from rh_partner where identifier=:p_identifier";

        $param = array(':p_identifier' => $p_identifier);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accSaveCustomer(int $p_partner): array{                                           
        $dataBase = new DataBaseKlinik($this->logger);   

        $sql = "insert into arc_customer (partner_id, activo) values (:p_partner, true)";

        $param = array(':p_partner' => $p_partner);

        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;

    }

    public function accSaveSupplier(int $p_partner): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "insert into aps_supplier (partner_id, activo) values (:p_partner, true)";

        $param = array(':p_partner' => $p_partner);

        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;

    }

    public function accUpdateProv(
                                    string $p_identifier,
                                    string $p_identifier_dv,                                    
                                    string $p_name,
                                    int $p_usuario,
                                    int $p_city,
                                    string $p_direccion,
                                    string $p_phone,
                                    string $p_mobile,
                                    string $p_email,
                                    string $p_giro,
                                    string $p_sucursal,
                                    string $p_sucursal_email,
                                    int $p_id
                                ): bool{
                                    
        $dataBase = new DataBaseKlinik($this->logger);                             

        $sql = "update rh_partner a 
                set 
                    identifier = :p_identifier,
                    identifier_dv = :p_identifier_dv,                 
                    name = :p_name,
                    user_id = :p_usuario 
                    address_country_id = (select country_id from base_territorial where id = :p_city), 
                    address_city_id = :p_city,
                    address = :p_direccion,
                    phone = :p_phone,
                    mobile_phone = :p_mobile,
                    email = :p_email,
                    company_activity = :p_giro
                    sucursal = :p_sucursal,
                    sucursal_email = :p_sucursal_email
                where id = :p_id and user_id = 1";

        $param = array(
            ':p_identifier' => $p_identifier,
            ':p_identifier_dv' => $p_identifier_dv,
            ':p_name' => $p_name,
            ':p_usuario' => $p_usuario,
            ':p_city' => $p_city,
            ':p_direccion' => $p_direccion,
            ':p_phone' => $p_phone,
            ':p_mobile' => $p_mobile,
            ':p_email' => $p_email,
            ':p_giro' => $p_giro,
            ':p_sucursal' => $p_sucursal,
            ':p_sucursal_email' => $p_sucursal_email,
            ':p_id' => $p_id
        );

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;      
    }

    public function accUpdateSupplier(int $p_id, string $p_proveedor): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
        $sql = "";
        if($p_proveedor==="T"){        
            $sql = "update aps_supplier set activo=true where partner_id = :p_id";
        }else{    
            $sql = "update aps_supplier set activo=false where partner_id = :p_id";
        }

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function accUpdateCustomer(int $p_id, string $p_cliente): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
        
        if($p_cliente==="T"){
            $sql = "update arc_customer set activo=true  where partner_id = :p_id";
        }else{
            $sql = "update arc_customer set activo=false where partner_id = :p_id";
        }  

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function accValidateSupplier(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "select count(*) as supplier from aps_supplier where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accDeleteSupplier(int $p_id): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "update aps_supplier set activo=false where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function accValidateCustomer(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "select count(*) as customer from arc_customer where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accDeleteCustomer(int $p_id): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "update arc_customer set activo=false where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function accGetAccounts(): array{                                           
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "SELECT id, account_code as cuenta, name FROM public.acc_account ORDER BY id ASC";

        $param = array();

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accGetDataPartner(int $p_id): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select (a.identifier||'-'||a.identifier_dv) as rut, a.name as nombre, a.company_activity as giro, a.address as direccion,
                (select name from base_territorial where id = a.address_city_id and char_length(code)=5) as ciudad
                from rh_partner a
                where id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function accValReportDiaryBookDate(string $p_fechaini, string $p_fechafin): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select count(*) as reports from acc_entry where to_char(entry_date,'dd-mm-yyyy') between :p_fechaini and :p_fechafin";

        $param = array(':p_fechaini' => $p_fechaini,':p_fechafin' => $p_fechafin);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function accReportDiaryBookDate(string $p_fechaini, string $p_fechafin): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select a.id, a.entry_code as comprobante, to_char(a.entry_date,'dd-mm-yyyy') as fecha,
                (select voucher_type_code from acc_voucher_type where id=a.voucher_type_id) as tipo 
                from acc_entry where to_char(entry_date,'dd-mm-yyyy') between :p_fechaini and :p_fechafin";

        $param = array(':p_fechaini' => $p_fechaini,':p_fechafin' => $p_fechafin);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValReportDiaryBookPeriod(string $p_periodo): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select count(*) as reports 
                from acc_entry a where to_char(a.entry_date,'mm-yyyy') = :p_periodo";

        $param = array(':p_periodo' => $p_periodo);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function accReportDiaryBookPeriod(string $p_periodo): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select a.id, a.entry_code as comprobante, to_char(a.entry_date,'dd-mm-yyyy') as fecha,
                (select voucher_type_code from acc_voucher_type where id=a.voucher_type_id) as tipo
                from acc_entry a where to_char(a.entry_date,'mm-yyyy') = :p_periodo";

        $param = array(':p_periodo' => $p_periodo);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accReportDiaryBookDetail(int $p_id): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select (select account_code from acc_account where id=a.account_id) as nro_cuenta,
		               (select name from acc_account where id=a.account_id) as nombre_cuenta,
                        a.debit as debito, 
                        a.credit as credito, 
                        a.description as glosa
                from acc_entry_detail a 
                where a.entry_id = :p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValPeriodReportBookBig(string $p_periodo): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select id from acc_entry where to_char(entry_date,'mm-yyyy') = :p_periodo";

        $param = array(':p_periodo' => $p_periodo);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValAccountReportBookBigDet(int $p_id, int $p_account): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select account_id from acc_entry_detail where entry_id=:p_id and account_id=:p_account";

        $param = array(':p_id' => $p_id,':p_account' => $p_account);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValDateReportBookBig(string $p_fechaini, string $p_fechafin): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select id from acc_entry where to_char(entry_date,'dd-mm-yyyy') between :p_fechaini and :p_fechafin";

        $param = array(':p_fechaini' => $p_fechaini, ':p_fechafin' => $p_fechafin);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accDetAccounts(int $p_id): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select a.account_code as num_cuenta, a.name as nombre,
                (select name from acc_account where id=a.parent_id) as num_mayor,
                (select name from acc_account where id=a.parent_id) as nombre_mayor 
                from acc_account a where id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accDetailsReportBookBig(int $p_id,int $p_cuenta): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select (select to_char(entry_date,'dd-mm-yyyy') from acc_entry where id=a.entry_id) as fecha, 
                a.description as descripcion, coalesce(a.debit,0) as debito, coalesce(a.credit,0) as credito,
                a.current_document_id as numero,
                (select voucher_type_code from acc_voucher_type where id=(select voucher_type_id from acc_entry where id=a.entry_id)) as tipo  
                from acc_entry_detail a 
                where a.entry_id=:p_id and a.account_id=:p_cuenta";

        $param = array(':p_id' => $p_id,':p_cuenta' => $p_cuenta);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }


}