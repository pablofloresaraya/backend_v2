<?php
declare(strict_types=1);

namespace App\Datos\Engine;

use Psr\Log\LoggerInterface;
use App\Datos\Connection\DataBaseKlinik;

class EngineSql{

    //private $dataBase;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        //$this->dataBase = new DataBaseKlinik();
        $this->logger = $logger;
    }

    public function getModules(string $p_type): array{   
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where parent_id is null
                and menu_type=:p_type
                and is_active=true
                order by menu_order";

        $param = array(':p_type' => $p_type);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleAll(int $p_module): array{   
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and is_active=true
                and parent_id is not null
                order by menu_order";

        $param = array(':p_module' => $p_module);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleSections(int $p_module): array{   
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and parent_id=:p_module
                and is_active=true
                order by menu_order";

        $param = array(':p_module' => $p_module);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleSectionMenu(int $p_module, int $p_section): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and parent_id=:p_section
                and is_active=true
                order by menu_order";

        $param = array(':p_module' => $p_module, ':p_section' => $p_section);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }
}
