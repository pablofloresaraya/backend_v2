<?php
declare(strict_types=1);

namespace App\Negocio;

use App\Datos\Engine\EngineSql;
use Psr\Log\LoggerInterface;

class Engine{
    protected $logger;

    public function __construct(array $p_data,LoggerInterface $logger){
        $this->logger = $logger;
    }

    public function getModules(string $p_type): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModules($p_type);
        $result = $dataset;
        return $result;
    } 

    public function getModuleAll(int $p_module): array{ 
        $result = array();
        if($p_module==-1){
            return $this->response("","error","modulo no valido",$result);
        }
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModuleAll($p_module);
        $result = $dataset;
        return $this->response("","ok","",$result);
    } 

    public function getModuleSections(int $p_module): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModuleSections($p_module);
        $result = $dataset;
        return $result;
    } 

    public function getModuleSectionMenu(int $p_module, int $p_section): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModuleSectionMenu($p_module, $p_section);
        $result = $dataset;
        return $result;
    } 

    private function response(string $p_header, string $p_status, string $p_message, array $p_data): array{
        return array("header" => $p_header, "status" => $p_status, "message" => $p_message, "data" => $p_data);
    }
}