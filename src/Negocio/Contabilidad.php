<?php
declare(strict_types=1);

namespace App\Negocio;

use App\Datos\Contabilidad\ContabilidadSql;
use Psr\Log\LoggerInterface;

class Contabilidad{
    protected $company_id;
    protected $logger;

    public function __construct(array $p_data,LoggerInterface $logger){
        $this->company_id = (int) $p_data["company_id"];
        $this->logger = $logger;
    }

    public function accVoucherTypeListAll(): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accVoucherTypeListAll($this->company_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accAccountListAll(string $p_name): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accAccountListAll($this->company_id,$p_name);
        $result = $this->accAccountCreateTree($dataset,0);
        return $this->response("","ok","",$result);
    }

    public function accAccountCreateTree(array $dataset,int $p_parent_id): array{
        $row = 0;
        $data = array();
        for($i=0;$i<count($dataset);$i++){
            if(empty($dataset[$i]['parent_id']) && $p_parent_id==0){
                $data[$row] = $dataset[$i];
                //$data[$row]['text'] = substr($dataset[$i]['name'],0,80);
                $data[$row]['text'] = $dataset[$i]['name'];
                $data[$row]['text_large'] = $dataset[$i]['name'];
                $data[$row]['children'] = $this->accAccountCreateTree($dataset,$dataset[$i]['id']);
                $row++;
            }
            else if($dataset[$i]['parent_id']==$p_parent_id){
                $data[$row] = $dataset[$i];
                //$data[$row]['text'] = substr($dataset[$i]['name'],0,80);
                $data[$row]['text'] = $dataset[$i]['name'];
                $data[$row]['text_large'] = $dataset[$i]['name'];

                if($dataset[$i]['type_name']=='group'){
                    $data[$row]['children'] = $this->accAccountCreateTree($dataset,$dataset[$i]['id']);
                    if(count($data[$row]['children'])==0){
                        $data[$row]['children'] = array(array('id'=>0, 'text'=>'Ninguna', 'isLeaf'=>true));
                    }
                }                    
                else
                    $data[$row]['isLeaf'] = true;

                $row++;
            }
        }
        return $data;
    }

    //add 19-07-21

    public function accRutTypeListAll(): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accRutTypeListAll($this->company_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accRegionsListAll(): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accRegionsListAll($this->company_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accProvincesListAll(int $p_region): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accProvincesListAll($p_region);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accCommunesListAll(int $p_provincia): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accCommunesListAll($p_provincia);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accGetProv(string $p_rut): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accGetProv($p_rut);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValidaProv(string $p_rut): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValidaProv($p_rut);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accSaveProv(
                            string $p_identifier,
                            string $p_dv,
                            string $p_name,
                            int $p_usuario,                                
                            int    $p_city,                               
                            string $p_direccion,
                            string $p_phone,
                            string $p_mobile,
                            string $p_email,
                            string $p_giro,
                            string $p_sucursal,
                            string $p_sucursal_email 
                                ): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accSaveProv(
                                                $p_identifier,
                                                $p_dv,
                                                $p_name,
                                                $p_usuario,                                
                                                $p_city,                               
                                                $p_direccion,
                                                $p_phone,
                                                $p_mobile,
                                                $p_email,
                                                $p_giro,
                                                $p_sucursal,
                                                $p_sucursal_email 
                                            );
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function accGetIdPartnerMax(): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accGetIdPartnerMax();
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accGetIdPartner(string $p_identifier): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accGetIdPartner($p_identifier);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accSaveCustomer(int $p_partner): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accSaveCustomer($p_partner);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accSaveSupplier(int $p_partner): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accSaveSupplier($p_partner);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accUpdateProv(
        string $p_identifier,
        string $p_identifier_dv,                                   
        string $p_name,
        int $p_usuario,
        int $p_city,
        string $p_direccion,
        string $p_phone,
        string $p_mobile,
        string $p_email,
        string $p_giro,
        string $p_sucursal,
        string $p_sucursal_email,
        int $p_id
    ): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accUpdateProv(
                                                $p_identifier,
                                                $p_identifier_dv,                                   
                                                $p_name,
                                                $p_usuario,
                                                $p_city,
                                                $p_direccion,
                                                $p_phone,
                                                $p_mobile,
                                                $p_email,
                                                $p_giro,
                                                $p_sucursal,
                                                $p_sucursal_email,
                                                $p_id
                                            );
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function accUpdateSupplier(int $p_id, string $p_proveedor): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accUpdateSupplier($p_id, $p_proveedor);
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function accUpdateCustomer(int $p_id, string $p_cliente): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accUpdateCustomer($p_id, $p_cliente);
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function accValidateSupplier(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValidateSupplier($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accDeleteSupplier(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accDeleteSupplier($p_id);
        $result = ['resp' => $dataset];
        return $this->response("","ok","",$result);
    }

    public function accValidateCustomer(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValidateCustomer($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accDeleteCustomer(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accDeleteCustomer($p_id);
        $result = ['resp' => $dataset];
        return $this->response("","ok","",$result);
    }

    public function accGetAccounts(): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accGetAccounts();
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accGetDataPartner(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accGetDataPartner($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValReportDiaryBookDate(string $p_fechaini, string $p_fechafin): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValReportDiaryBookDate($p_fechaini, $p_fechafin);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accReportDiaryBookDate(string $p_fechaini, string $p_fechafin): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accReportDiaryBookDate($p_fechaini, $p_fechafin);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValReportDiaryBookPeriod(string $p_periodo): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValReportDiaryBookPeriod($p_periodo);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accReportDiaryBookPeriod(string $periodo): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accReportDiaryBookPeriod($periodo);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accReportDiaryBookDetail(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accReportDiaryBookDetail($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValPeriodReportBookBig(string $p_periodo): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValPeriodReportBookBig($p_periodo);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValAccountReportBookBigDet(int $p_id, int $p_account): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValAccountReportBookBigDet($p_id, $p_account);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValDateReportBookBig(string $p_fechaini, string $p_fechafin): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValDateReportBookBig($p_fechaini, $p_fechafin);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accDetAccounts(int $p_id): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accDetAccounts($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accDetailsReportBookBig(int $p_id,int $p_cuenta): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accDetailsReportBookBig($p_id,$p_cuenta);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    private function response(string $p_header, string $p_status, string $p_message, array $p_data): array{
        return array("header" => $p_header, "status" => $p_status, "message" => $p_message, "data" => $p_data);
    }

    /*
    public function accVoucherTypeView(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accVoucherTypeUpdate(array $originData, array $formData): array{
        $id = $formData['key1'];
        $data["company_id"] = $formData['key2'];
        $data["voucher_type_code"] = $formData['key3'];
        $data["name"] = $formData['key4'];
        $data["description"] = $formData['key5'];
        $data["class_type"] = $formData['key6'];

        if (!isset($id) || empty($id) || count($originData)==0) {
            throw new ContabilidadNotFoundException();
        }

        $dataset = $originData;
        
        if($originData['company_id']!=$data["company_id"]){
            $dataset['company_id'] = $data["company_id"];
        }        
        if($originData['voucher_type_code']!=$data["voucher_type_code"]){
            $dataset['voucher_type_code'] = $data["voucher_type_code"];
        }        
        if($originData['name']!=$data["name"]){
            $dataset['name'] = $data["name"];
        }        
        if($originData['description']!=$data["description"]){
            $dataset['description'] = $data["description"];
        }        
        if($originData['class_type']!=$data["class_type"]){
            $dataset['class_type'] = $data["class_type"];
        }
        
        return  array($dataset['id'], $dataset['company_id'], $dataset['voucher_type_code'], $dataset['name'], $dataset['description'], $dataset['class_type']);
    }

    public function jsonSerialize()
    {
        return [
            
        ];
    }
    */   
}