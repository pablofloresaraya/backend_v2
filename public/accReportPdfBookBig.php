<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//use Firebase\JWT\JWT;
use Dotenv\Dotenv;
//use Tuupola\Base62;
use App\Negocio\Token;
use App\Negocio\Contabilidad;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

require __DIR__ . '/../vendor/autoload.php';
$php_input=json_decode(file_get_contents('php://input'),1);

$dotenv = Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();

$loggerPath  = $_ENV["LOG"];
$servername  = $_ENV["SERVERNAME"];
$basepath    = $_ENV["BASEPATH"];
$secret      = $_ENV["JWT_SECRET"];
$algorithm   = $_ENV["JWT_ALGORITHM"];
$hrs_expired = $_ENV["JWT_HRS_EXPIRED"];
$username    = "admin";

$loggerSettings = array(
    'name' => 'klinik-backend',
    'path' => '/var/www/html/backend/logs/app.log',
    'level' => Logger::DEBUG,
);

$logger = new Logger($loggerSettings['name']);

$processor = new UidProcessor();
$logger->pushProcessor($processor);

$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
$logger->pushHandler($handler);

$jwt_data["algorithm"]   = $algorithm;
$jwt_data["secretkey"]   = $secret;
$jwt_data["servername"]  = $servername;
$jwt_data["username"]    = $username;
$jwt_data["hrs_expired"] = $hrs_expired;

$obj_token = new Token($jwt_data,$logger);

$param_c = (empty($_GET["c"]))? '':base64_decode($_GET["c"]); 

if(empty($_GET["t"])){
    $logger->error("token no existe");
    exit;    
}

$param_t = $_GET["t"];

$token_validate = $obj_token->validateToken($param_t);
$logger->info("Estado token: ",$token_validate);
if($token_validate["status"]=="ERROR"){
    header($token_validate["header"]);
    exit;
}

$logger->info("Iniciando clases de negocios");

$param1["company_id"] = $param_c;
$contabilidad = new Contabilidad($param1,$logger);

//init reports

require_once __DIR__ .'/dompdf/autoload.inc.php';

ob_start();

use Dompdf\Dompdf;

$p_fechaini  = (empty($_GET["i"]))? '':base64_decode($_GET["i"]);
$p_fechafin  = (empty($_GET["f"]))? '':base64_decode($_GET["f"]);
$p_periodo   = (empty($_GET["p"]))? '':base64_decode($_GET["p"]);
$p_cuenta    = (empty($_GET["a"]))? '':base64_decode($_GET["a"]);
$p_cuentagen = (empty($_GET["g"]))? '':base64_decode($_GET["g"]);

$partner = $contabilidad->accGetDataPartner($p_cuentagen);

$cabecera = array();

if(empty($p_periodo)){ //reporte por fechas

    $cabecera = $contabilidad->accValDateReportBookBig($p_fechaini,$p_fechafin);

}else{ //reporte por periodo

    $cabecera = $contabilidad->accValPeriodReportBookBig($p_periodo);

}

$cuentas = $contabilidad->accDetAccounts($p_cuenta); //mayor-subcuenta

$cuenta_mayor_num = empty($cuentas['data'][0]['num_mayor']) ? $cuentas['data'][0]['num_cuenta'] : $cuentas['data'][0]['num_mayor'];
$cuenta_mayor_nom = empty($cuentas['data'][0]['nombre_mayor']) ? $cuentas['data'][0]['nombre'] : $cuentas['data'][0]['nombre_mayor'];

//css
/*table {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid black;
}*/
$html="<style>
        .separator-bottom {
            border-bottom: 1px solid black;
        }
    </style>";

date_default_timezone_set("America/Santiago");
$hora = date("G:i");
   
    $html.="<table width='100%' >
            <tr>
                <td colspan='2'><b>".$partner['data'][0]['nombre']."</b></td>
                <td>P&aacute;gina 1</td>
            </tr>
            <tr>
                <td style='width:15%'>R.U.T.</td>
                <td style='text-align: left; width:73%'>".$partner['data'][0]['rut']."</td>
                <td style='width:12%'>Hora: ".$hora."</td>
            </tr>
            <tr>
                <td>GIRO</td>
                <td>".$partner['data'][0]['giro']."</td>
                <td></td>
            </tr>
            <tr>
                <td>DIRECCI&Oacute;N</td>
                <td>".$partner['data'][0]['direccion']."</td>
                <td></td>
            </tr>
            <tr>
                <td>CIUDAD</td>
                <td style='text-align: left'>".$partner['data'][0]['ciudad']."</td>
                <td></td>
            </tr>
        </table>
        <table width='100%'>
            <tr>
                <td style='text-align: center' colspan=2><b>LIBRO MAYOR</b></td>
            </tr>
            <tr>
                <td style='width:21%'>Cuenta Mayor N&#176;</td>
                <td>".$cuenta_mayor_num." ".$cuenta_mayor_nom."</td>
            </tr>
            <tr>
                <td>Sub-Cuenta Mayor N&#176;</td>
                <td>".$cuentas['data'][0]['num_cuenta'] ." ".$cuentas['data'][0]['nombre']."</td>
            </tr>
        </table>
        </br>
        <table width='100%'>
            <tr>
                <th class='separator-bottom'>Fecha</th>
                <th class='separator-bottom'>Tipo</th>
                <th class='separator-bottom'>N&uacute;mero</th>
                <th class='separator-bottom'>Tipo</th>
                <th class='separator-bottom'>N&uacute;mero</th>
                <th class='separator-bottom'>Glosa</th>
                <th class='separator-bottom'>DEBITO</th>
                <th class='separator-bottom'>CREDITO</th>
                <th class='separator-bottom'>SALDO</th>
            </tr>";

            $total_debito  = 0;
            $total_credito = 0;
            $total_saldo   = 0;

            for($i=0;$i<count($cabecera['data']);$i++){
                                                            
                $details = $contabilidad->accDetailsReportBookBig($cabecera['data'][$i]['id'],$p_cuenta);

                for($n=0;$n<count($details['data']);$n++){

                    $html.="
                        <tr>
                            <td>".$details['data'][$n]['fecha']."</td>
                            <td>".$details['data'][$n]['tipo']."</td>
                            <td>".$details['data'][$n]['numero']."</td>
                            <td></td>
                            <td style='text-align:right'>0</td>
                            <td>".$details['data'][$n]['descripcion']."</td>
                            <td style='text-align:right'>".$details['data'][$n]['debito']."</td>
                            <td style='text-align:right'>".$details['data'][$n]['credito']."</td>
                            <td style='text-align:right'>".($details['data'][$n]['debito']-$details['data'][$n]['credito'])."</td>
                        </tr>
                    ";

                    $total_debito  += $details['data'][$n]['debito'];
                    $total_credito += $details['data'][$n]['credito'];
                    $total_saldo   += ($details['data'][$n]['debito']-$details['data'][$n]['credito']);
                }

            }

$html.="<tr style='text-align:right'>
            <td colspan=6><b>TOTAL CUENTA(S)</b></td>
            <td>".$total_debito."</td>
            <td>".$total_credito."</td>
            <td>".$total_saldo."</td>
        </tr></table>";

$dompdf = new DOMPDF();
$dompdf->loadHtml($html);
$dompdf->render();
$dompdf->stream("report.pdf");