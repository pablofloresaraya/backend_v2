<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//use Firebase\JWT\JWT;
use Dotenv\Dotenv;
//use Tuupola\Base62;
use App\Negocio\Token;
use App\Negocio\Contabilidad;
use App\Negocio\Engine;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

require __DIR__ . '/../vendor/autoload.php';
$php_input=json_decode(file_get_contents('php://input'),1);
//$data["php_post"]=$_POST;
//$data["php_files"]=$_FILES;

$dotenv = Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();

$loggerPath = $_ENV["LOG"];
$servername = $_ENV["SERVERNAME"];
$basepath = $_ENV["BASEPATH"];
$secret = $_ENV["JWT_SECRET"];
$algorithm = $_ENV["JWT_ALGORITHM"];
$hrs_expired = $_ENV["JWT_HRS_EXPIRED"];
$username = "admin";

$loggerSettings = array(
    'name' => 'klinik-backend',
    'path' => '/var/www/html/backend/logs/app.log',
    'level' => Logger::DEBUG,
);

$logger = new Logger($loggerSettings['name']);

$processor = new UidProcessor();
$logger->pushProcessor($processor);

$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
$logger->pushHandler($handler);

$jwt_data["algorithm"] = $algorithm;
$jwt_data["secretkey"] = $secret;
$jwt_data["servername"] = $servername;
$jwt_data["username"] = $username;
$jwt_data["hrs_expired"] = $hrs_expired;

$obj_token = new Token($jwt_data,$logger);

//if(!empty($_GET)){
//    $param_r = (empty($_GET["r"]))? '':base64_decode($_GET["r"]);
//    $param_o = (empty($_GET["o"]))? '':base64_decode($_GET["o"]);
//    $param_t = (empty($_GET["o"]))? '':base64_decode($_GET["t"]);
//}
//else if(!empty($_POST)){
//    $param_r = (empty($_POST["r"]))? '':base64_decode($_POST["r"]);
//    $param_o = (empty($_POST["o"]))? '':base64_decode($_POST["o"]);  
//    $param_t = (empty($_POST["o"]))? '':base64_decode($_POST["t"]);  
//}
$param_r = (empty($_GET["r"]))? '':base64_decode($_GET["r"]);
$param_o = (empty($_GET["o"]))? '':base64_decode($_GET["o"]);  
$param_c = (empty($_GET["c"]))? '':base64_decode($_GET["c"]);  

$esLogin = ($param_r=="login")? true:false;
if($esLogin){
    //Validar usuario y contraseña. Si pasa validación crear token
    $token = $obj_token->encode();
    $token["user"] = "admin";
    $token["name"] = "Administrador";    
    
    echo json_encode($token);
}
else{
    if(empty($_GET["t"])){
        $logger->error("token no existe");
        exit;    
    }

    $param_t = $_GET["t"];
    //$logger->info("php_input: ".$php_input["o"],$php_input);
    //$logger->info("php_get: ",$_GET);
    //$logger->info("php_post: ",$_POST);
    //$logger->info("Cabecera: ",getallheaders());
    //$logger->info("Cabecera: ",apache_request_headers());
    //$header_data = getallheaders(); jelgueda: comentado hasta que sepa por qué se pierde el header Authorization cuando llamo desde fetch. Desde postman funciona bien
    //$token_validate = $obj_token->validate($header_data);
    $token_validate = $obj_token->validateToken($param_t);
    $logger->info("Estado token: ",$token_validate);
    if($token_validate["status"]=="ERROR"){
        header($token_validate["header"]);
        exit;
    }
    
    $logger->info("Iniciando clases de negocios");

    $param1["company_id"] = $param_c;
    $contabilidad = new Contabilidad($param1,$logger);
    $param2["company_id"] = $param_c;
    $engine = new Engine($param2,$logger);

    $logger->info("route: ".$param_r." ".$param_o);
    switch($param_r){
        case "vouchertypes":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/accVoucherTypeListAll.php";
                    break;
            }
            break;
        case "module":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/engGetModuleAll.php";
                    break;
            }
            break;
        case "accounts":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/accAccountListAll.php";
                    break;
            }
            break;
        case "accounting":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/accAccountinsEntryImportDetail.php";
                    break;
                case "2":
                   // include_once $basepath."/src/Presentacion/accRutListAll.php";
                    break;
                case "3":
                    include_once $basepath."/src/Presentacion/accAccountBookListAll.php";
                    break;
                case "4":
                    //include_once $basepath."/src/Presentacion/accGetSupplierCustomerListAll.php";
                    break;
                case "5":
                    //include_once $basepath."/src/Presentacion/accAccountingPartnerSave.php";
                    break;
                case "6":
                    include_once $basepath."/src/Presentacion/accDeleteSupplierCustomer.php";
                    break;
                case "7":
                    include_once $basepath."/src/Presentacion/accGetPathReportBookDiary.php";                    
                    break;
                case "8":
                    include_once $basepath."/src/Presentacion/accGetPathReportBookBig.php";                    
                    break;               
            }
            break;
        case "partners":
            switch($param_o){
                case "2":
                    include_once $basepath."/src/Presentacion/rhPartnerListAll.php";
                    break;
                case "4":
                    include_once $basepath."/src/Presentacion/rhPartnerView.php";
                    break;
                case "5":
                    include_once $basepath."/src/Presentacion/rhPartnerSave.php";
                    break;
            }
            break;
        case "territorial":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/accTerritorialRegionsList.php";
                    break;
                case "2":
                    include_once $basepath."/src/Presentacion/accTerritorialProvincesList.php";
                    break;
                case "3":
                    include_once $basepath."/src/Presentacion/accTerritorialCommunesList.php";
                    break;    
            }
            break;    
    }    
}

?>