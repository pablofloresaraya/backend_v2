<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

use Dotenv\Dotenv;
use App\Negocio\Token;
use App\Negocio\Contabilidad;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

require __DIR__ . '/../vendor/autoload.php';
$php_input=json_decode(file_get_contents('php://input'),1);

$dotenv = Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();

$loggerPath  = $_ENV["LOG"];
$servername  = $_ENV["SERVERNAME"];
$basepath    = $_ENV["BASEPATH"];
$secret      = $_ENV["JWT_SECRET"];
$algorithm   = $_ENV["JWT_ALGORITHM"];
$hrs_expired = $_ENV["JWT_HRS_EXPIRED"];
$username    = "admin";

$loggerSettings = array(
    'name' => 'klinik-backend',
    'path' => '/var/www/html/backend/logs/app.log',
    'level' => Logger::DEBUG,
);

$logger = new Logger($loggerSettings['name']);

$processor = new UidProcessor();
$logger->pushProcessor($processor);

$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
$logger->pushHandler($handler);

$jwt_data["algorithm"]   = $algorithm;
$jwt_data["secretkey"]   = $secret;
$jwt_data["servername"]  = $servername;
$jwt_data["username"]    = $username;
$jwt_data["hrs_expired"] = $hrs_expired;

$obj_token = new Token($jwt_data,$logger);

$param_c = (empty($_GET["c"]))? '':base64_decode($_GET["c"]); 

if(empty($_GET["t"])){
    $logger->error("token no existe");
    exit;    
}

$param_t = $_GET["t"];

$token_validate = $obj_token->validateToken($param_t);
$logger->info("Estado token: ",$token_validate);
if($token_validate["status"]=="ERROR"){
    header($token_validate["header"]);
    exit;
}

$logger->info("Iniciando clases de negocios");

$param1["company_id"] = $param_c;
$contabilidad = new Contabilidad($param1,$logger);

$p_fechaini  = (empty($_GET["i"]))? '':base64_decode($_GET["i"]);
$p_fechafin  = (empty($_GET["f"]))? '':base64_decode($_GET["f"]);
$p_periodo   = (empty($_GET["p"]))? '':base64_decode($_GET["p"]);
$p_cuenta    = (empty($_GET["a"]))? '':base64_decode($_GET["a"]); //id
$p_cuentagen = (empty($_GET["g"]))? '':base64_decode($_GET["g"]);

$partner = $contabilidad->accGetDataPartner($p_cuentagen);

$cabecera = array();

if(empty($p_periodo)){ //reporte por fechas

    $cabecera = $contabilidad->accValDateReportBookBig($p_fechaini,$p_fechafin);

}else{ //reporte por periodo

    $cabecera = $contabilidad->accValPeriodReportBookBig($p_periodo);

}

$cuentas = $contabilidad->accDetAccounts($p_cuenta); //mayor-subcuenta

$cuenta_mayor_num = empty($cuentas['data'][0]['num_mayor']) ? $cuentas['data'][0]['num_cuenta'] : $cuentas['data'][0]['num_mayor'];
$cuenta_mayor_nom = empty($cuentas['data'][0]['nombre_mayor']) ? $cuentas['data'][0]['nombre'] : $cuentas['data'][0]['nombre_mayor'];

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$spreadsheet = new Spreadsheet();

date_default_timezone_set("America/Santiago");
$hora = date("G:i");

$spreadsheet->getProperties()
    ->setTitle('PHP Download Example')
    ->setSubject('A PHPExcel example')
    ->setDescription('A simple example for PhpSpreadsheet. This class replaces the PHPExcel class')
    ->setCreator('php-download.com')
    ->setLastModifiedBy('php-download.com');

$spreadsheet->setActiveSheetIndex(0)->mergeCells('A1:H1');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('B2:H2');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('B3:H3');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('B4:H4');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('B5:H5');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('A6:I6');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('B7:I7');
$spreadsheet->setActiveSheetIndex(0)->mergeCells('B8:I8');

$spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', $partner['data'][0]['nombre'])
    ->SetCellValue('I1', "Página 1")
    ->SetCellValue('I2', "Hora: ".$hora)
    ->SetCellValue('A2', "R.U.T :")
    ->SetCellValue('B2', $partner['data'][0]['rut'])
    ->SetCellValue('A3', "GIRO :")
    ->SetCellValue('B3', $partner['data'][0]['giro'])
    ->SetCellValue('A4', "DIRECCIÓN :")
    ->SetCellValue('B4', $partner['data'][0]['direccion'])
    ->SetCellValue('A5', "CIUDAD :")
    ->SetCellValue('B5', $partner['data'][0]['ciudad'])
    ->SetCellValue('A6', "LIBRO MAYOR")
    ->SetCellValue('A7', "Cuenta Mayor N°")
    ->SetCellValue('B7', $cuenta_mayor_num.' '.$cuenta_mayor_nom)
    //->SetCellValue('H7', "Correlativo:")
    ->SetCellValue('A8', "Sub-Cuenta Mayor N°")
    ->SetCellValue('B8', $cuentas['data'][0]['num_cuenta'].' '.$cuentas['data'][0]['nombre'])
    ->SetCellValue('A10', "Fecha")
    ->SetCellValue('B10', "Tipo")
    ->SetCellValue('C10', "Número")
    ->SetCellValue('D10', "Tipo")
    ->SetCellValue('E10', "Número")
    ->SetCellValue('F10', "Glosa")
    ->SetCellValue('G10', "DEBITO")
    ->SetCellValue('H10', "CREDITO")
    ->SetCellValue('I10', "SALDO");

$spreadsheet->getActiveSheet()->getStyle('A6:I6')->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);

$spreadsheet->getActiveSheet()->getStyle('A10:I10')->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);

$spreadsheet->getActiveSheet()->getStyle('A6:I6')->getAlignment()->setHorizontal('center');

//cell size automatic
foreach(range('A2','A5') as $columnID){
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

foreach(range('A7','A8') as $columnID){
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$total_debito=0;
$total_credito=0;
$total_saldo=0;
$j=10;

for($i=0;$i<count($cabecera['data']);$i++){
                                                            
    $details = $contabilidad->accDetailsReportBookBig($cabecera['data'][$i]['id'],$p_cuenta);

    for($n=0;$n<count($details['data']);$n++){

        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.($j+1), $details['data'][$n]['fecha'])
        ->setCellValue('B'.($j+1), $details['data'][$n]['tipo'])
        ->setCellValue('C'.($j+1), $details['data'][$n]['numero'])
        ->setCellValue('D'.($j+1), "")
        ->setCellValue('E'.($j+1), "")
        ->setCellValue('F'.($j+1), $details['data'][$n]['descripcion'])
        ->setCellValue('G'.($j+1), $details['data'][$n]['credito'])
        ->setCellValue('H'.($j+1), $details['data'][$n]['debito'])
        ->setCellValue('I'.($j+1), ($details['data'][$n]['debito']-$details['data'][$n]['credito']));

        $total_debito  += $details['data'][$n]['debito'];
        $total_credito += $details['data'][$n]['credito'];
        $total_saldo   += ($details['data'][$n]['debito']-$details['data'][$n]['credito']);
        $j++;

    }

}

foreach(range('F11','F'.$j) as $columnID){
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$spreadsheet->setActiveSheetIndex(0)->mergeCells('A'.($j+1).':F'.($j+1));
$spreadsheet->getActiveSheet()->getStyle('A'.($j+1).':F'.($j+1))->getAlignment()->setHorizontal('center');
$spreadsheet->getActiveSheet()->getStyle('A'.($j+1).':F'.($j+1))->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);

$spreadsheet->setActiveSheetIndex(0)
->setCellValue('A'.($j+1), 'TOTAL CUENTA(S)')
->setCellValue('G'.($j+1), $total_credito)
->setCellValue('H'.($j+1), $total_debito)
->setCellValue('I'.($j+1), $total_saldo);

foreach(range('G11','G'.$j) as $columnID){
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

foreach(range('H11','H'.$j) as $columnID){
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$writer = IOFactory::createWriter($spreadsheet, "Xlsx"); //Xls is also possible
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="excel.xlsx"');
header('Cache-Control: max-age=0');
$writer->save('php://output');

