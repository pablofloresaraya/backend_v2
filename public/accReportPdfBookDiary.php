<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//use Firebase\JWT\JWT;
use Dotenv\Dotenv;
//use Tuupola\Base62;
use App\Negocio\Token;
use App\Negocio\Contabilidad;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

require __DIR__ . '/../vendor/autoload.php';
$php_input=json_decode(file_get_contents('php://input'),1);

$dotenv = Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();

$loggerPath  = $_ENV["LOG"];
$servername  = $_ENV["SERVERNAME"];
$basepath    = $_ENV["BASEPATH"];
$secret      = $_ENV["JWT_SECRET"];
$algorithm   = $_ENV["JWT_ALGORITHM"];
$hrs_expired = $_ENV["JWT_HRS_EXPIRED"];
$username    = "admin";

$loggerSettings = array(
    'name' => 'klinik-backend',
    'path' => '/var/www/html/backend/logs/app.log',
    'level' => Logger::DEBUG,
);

$logger = new Logger($loggerSettings['name']);

$processor = new UidProcessor();
$logger->pushProcessor($processor);

$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
$logger->pushHandler($handler);

$jwt_data["algorithm"]   = $algorithm;
$jwt_data["secretkey"]   = $secret;
$jwt_data["servername"]  = $servername;
$jwt_data["username"]    = $username;
$jwt_data["hrs_expired"] = $hrs_expired;

$obj_token = new Token($jwt_data,$logger);

$param_c = (empty($_GET["c"]))? '':base64_decode($_GET["c"]); 

if(empty($_GET["t"])){
    $logger->error("token no existe");
    exit;    
}

$param_t = $_GET["t"];

$token_validate = $obj_token->validateToken($param_t);
$logger->info("Estado token: ",$token_validate);
if($token_validate["status"]=="ERROR"){
    header($token_validate["header"]);
    exit;
}

$logger->info("Iniciando clases de negocios");

$param1["company_id"] = $param_c;
$contabilidad = new Contabilidad($param1,$logger);

//init reports

require_once __DIR__ .'/dompdf/autoload.inc.php';

ob_start();

use Dompdf\Dompdf;

$p_fechaini  = (empty($_GET["i"]))? '':base64_decode($_GET["i"]);
$p_fechafin  = (empty($_GET["f"]))? '':base64_decode($_GET["f"]);
$p_periodo   = (empty($_GET["p"]))? '':base64_decode($_GET["p"]);
$p_cuentagen = (empty($_GET["g"]))? '':base64_decode($_GET["g"]);

$partner = $contabilidad->accGetDataPartner($p_cuentagen);

$cabecera = array();

if(empty($p_periodo)){ //reporte por fechas

    $cabecera = $contabilidad->accReportDiaryBookDate($p_fechaini,$p_fechafin);

}else{ //reporte por periodo

    $cabecera = $contabilidad->accReportDiaryBookPeriod($p_periodo);

}

//css
$html="<style>
        table {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid black;
        }
        .separator {
            border-top: 1px solid black;
        }
    </style>";

date_default_timezone_set("America/Santiago");

//for($i=0; $i<count($partner['data']); $i++){

    $hora = date("G:i");

    $html.="<table >
            <tr>
                <td colspan='2'><b>".$partner['data'][0]['nombre']."</b></td>
                <td>P&aacute;gina 1</td>
            </tr>
            <tr>
                <td style='width:15%'>R.U.T.</td>
                <td style='text-align: left; width:73%'>".$partner['data'][0]['rut']."</td>
                <td style='width:12%'>Hora: ".$hora."</td>
            </tr>
            <tr>
                <td>GIRO</td>
                <td>".$partner['data'][0]['giro']."</td>
                <td></td>
            </tr>
            <tr>
                <td>DIRECCI&Oacute;N</td>
                <td>".$partner['data'][0]['direccion']."</td>
                <td></td>
            </tr>
            <tr>
                <td>CIUDAD</td>
                <td style='text-align: left'>".$partner['data'][0]['ciudad']."</td>
                <td></td>
            </tr>
            <tr>
                <td colspan=3 style='text-align: center'><b>LIBRO DIARIO</b></td>         
            </tr>
        </table>
        </br>
        <table>
            <tr>
                <th style='text-align: left'>Cta.</th>
                <th style='text-align: left'>Nombre</th>
                <th style='text-align: left'>Tipo</th>
                <th style='text-align: center'>N&uacute;mero</th>
                <th style='text-align: left'>Glosa</th>
                <th style='text-align: left'>DEBITO</th>
                <th style='text-align: left'>CREDITO</th>
            </tr>";

            $total_debito  = 0;
            $total_credito = 0;

            for($i=0;$i<count($cabecera['data']);$i++){
                
                $total_comprobante_debito  = 0;
                $total_comprobante_credito = 0;

                $html.= "<tr style='font-size: 10px'>
                            <td>&nbsp;</td>
                            <td>Comprobante: ".$cabecera['data'][$i]['comprobante']." Tipo: </td>
                            <td>".$cabecera['data'][$i]['tipo']."</td>
                            <td style='text-align: center'>Fecha: ".$cabecera['data'][$i]['fecha']."</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>";

                $detalles = $contabilidad->accReportDiaryBookDetail($cabecera['data'][$i]['id']);

                for($n=0;$n<count($detalles['data']);$n++){
                    
                    $html.="<tr style='font-size: 12px'>
                                <td>".$detalles['data'][$n]['nro_cuenta']."</td>
                                <td>".$detalles['data'][$n]['nombre_cuenta']."</td>
                                <td>TR</td>
                                <td style='text-align: right'>0</td>
                                <td>".$detalles['data'][$n]['glosa']."</td>
                                <td style='text-align: right'>".(empty($detalles['data'][$n]['debito']) ? "" : number_format($detalles['data'][$n]['debito'],0,'','.'))."</td>
                                <td style='text-align: right'>".(empty($detalles['data'][$n]['credito']) ? "" : number_format($detalles['data'][$n]['credito'],0,'','.'))."</td>
                            </tr>";

                    $total_comprobante_debito  += $detalles['data'][$n]['debito'];
                    $total_comprobante_credito += $detalles['data'][$n]['credito'];        

                }

                $html.="<tr style='font-size: 12px'>
                        <td colspan=4></td>
                        <td style='text-align: center'>TOTAL COMPROBANTE</td>
                        <td class='separator' style='text-align: right'>".number_format($total_comprobante_debito,0,'','.')."</td>
                        <td class='separator' style='text-align: right'>".number_format($total_comprobante_credito,0,'','.')."</td>
                        </tr>";

                $total_debito  += $total_comprobante_debito;        
                $total_credito += $total_comprobante_credito;        

            }

$html.="<tr style='font-size: 12px'>
            <td colspan=5></td>
            <td class='separator' style='text-align: right'>".number_format($total_debito,0,'','.')."</td>
            <td class='separator' style='text-align: right'>".number_format($total_credito,0,'','.')."</td>
        </tr></table>";

//<div style='page-break-after:always;'></div>        
//}    

/*echo $html;
exit; */ 

$dompdf = new DOMPDF();
$dompdf->loadHtml($html);
$dompdf->render();
//$output = $dompdf->output();
$dompdf->stream("report.pdf");










